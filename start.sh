#!/bin/bash
set -euo pipefail

generate_container_user() {
    local USER_ID; USER_ID="$(id -u)"
    if [ x"$USER_ID" != x"0" -a x"$USER_ID" != x"1000" ]; then
        sed -i '/^default:/'"s/x:1000:/x:$USER_ID:/" /etc/passwd
    fi
}

generate_container_user

export USER="$(whoami)"

mkdir -p "$HOME/.ssh"
chmod 0700 "$HOME/.ssh" || true

if [ -f /secrets/known_hosts ]; then
    cp /secrets/known_hosts $HOME/.ssh/known_hosts
fi

export AUTOSSH_DEBUG="${AUTOSSH_DEBUG-true}"

export AUTOSSH_LOGFILE=$HOME/log
rm -f "$AUTOSSH_LOGFILE"
mkfifo $AUTOSSH_LOGFILE

# log to stdout
tail -f $AUTOSSH_LOGFILE &

set -x
exec autossh -M 0 -o ServerAliveInterval=1 -o ServerAliveCountMax=3 \
    -i /secrets/ssh-privatekey "$@"
